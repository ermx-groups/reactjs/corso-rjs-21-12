## Esercizio

Creare un componente Form che abbia 3 
- input ed un pulsante submit 
- abbia uno stato con 3 proprietà (una per ogni input)
- ogni proprietà dello stato sia associata ad un input sia in visualizzazione che in modifica


## Esercizio 2

Implementare una nuova App da zero che abbia le stesse funzionalità viste a lezione.
Buon lavoro.