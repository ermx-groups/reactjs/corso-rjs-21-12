import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Component/App.js';
import App2 from './Esercizio/App.js';
ReactDOM.render(
  <>
    <App />
    <hr/>
    <App2/>
  </>,
  document.getElementById('root')
);
