import React from "react";
import "./Form.css";

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {nome : '', cognome:'', telefono:'' };

        this.handlerClick = this.handlerClick.bind(this);
    }
    handlerClick() {
        let item = {
            name : this.state.nome, 
            surname : this.state.cognome, 
            tel : this.state.telefono
        };
        this.props.onConfirm(item);
        this.setState({nome:'', cognome:'', telefono:''});
    }
    render() {
        return <div>
            <form>
            <input placeholder="Nome" name="nome" type="text" value={this.state.nome} onChange={(event) => this.setState({nome: event.target.value})}/>
            <input placeholder="Cognome" name="cognome" type="text" value={this.state.cognome} onChange={(event) => this.setState({cognome: event.target.value})}/>
            <input placeholder="Telefono" name="telefono" type="text" value={this.state.telefono} onChange={(event) => this.setState({telefono: event.target.value})}/>
            <input type="button" value="Conferma" onClick={this.handlerClick}/>
            </form>
        </div>
    }
}

export default Form;