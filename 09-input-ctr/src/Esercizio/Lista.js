import React from "react";

class Lista extends React.Component {
    delete(idDaCancellare) {
        this.props.onDelete(idDaCancellare);
    }
    render() {
        let dati = this.props.dati;
        return <div className="lista">
            <b>Lista Elementi</b>
            {dati.map(item => <div key={item.id} onClick={()=>this.delete(item.id)} className="item">{item.id}) {item.surname} {item.name} : {item.tel}</div>)}
        </div>
    }
}

export default Lista;