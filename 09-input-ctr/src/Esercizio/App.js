import React from "react";
import Form from "./Form";
import Lista from "./Lista";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.handlerConfirm = this.handlerConfirm.bind(this);
        this.handlerDelete = this.handlerDelete.bind(this);
        this.state = {lista : [], cc : 0};
    }
    handlerConfirm(item) {

        let nl = [...this.state.lista];
        item.id = this.state.cc + 1;
        nl.push(item);
        this.setState({lista : nl, cc : item.id});
    }

    handlerDelete(idCancellare) {
        let nl = [...this.state.lista];
        let lGiusta = nl.filter(item => item.id!=idCancellare);
        this.setState({lista : lGiusta});
    }
    render() {
        return <div>
            <Form onConfirm={this.handlerConfirm}/>
            <Lista dati={this.state.lista} onDelete={this.handlerDelete}/>
        </div>
    }
}

export default App;