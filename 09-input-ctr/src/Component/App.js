
import React from 'react';

import './App.css';
  import FormData from './FormData';
import ListData from './ListData';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {dati : []};
    this.handlerAddData = this.handlerAddData.bind(this);
  }
  
  handlerAddData(item) {
    let ns = [...this.state.dati, item];
    this.setState({dati:ns});
  }

  render() {
    let lista = [...this.state.dati];
    return (
      <div className="App">
        <div className="main">
          <FormData onConfirm={this.handlerAddData}/>
          <ListData data={lista}/>
        </div>
      </div>
    );  
  }
}

export default App;
