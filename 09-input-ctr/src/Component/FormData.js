import React from "react";
import E from "react-script";
function generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

class FormData extends React.Component {
    constructor(props) {
        super(props);
        this.handlerConfirm = this.handlerConfirm.bind(this);
        this.state = {name : '', number:''};
    }

    handlerConfirm() {
        let item = {... this.state, id:generateUUID()};
        this.props.onConfirm(item);
        this.setState({name:'', number:''});
    }
    render() {
        return <div>
            <form>
                <input value={this.state.name} placeholder="Name" onChange={(e)=>this.setState({name:e.target.value})}/>
                <input value={this.state.number} placeholder="Number" onChange={(e)=>this.setState({number:e.target.value})}/>
                <input type="button" onClick={this.handlerConfirm} value="Confirm"/>
            </form>
        </div>
    }
}

export default FormData;