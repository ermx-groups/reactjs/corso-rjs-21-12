import React from "react";

class ListData extends React.Component {
    render() {
        return <div className="lista">
            {this.props.data.map(item => <div className="item" key={item.id} id={item.id}> {item.name} : {item.number}</div>)}
        </div>
    }
}

export default ListData;