# Introduzione al CSS

Definizione degli scopi del linguaggio e descrizione della sua sintassi

## Proposte di studio
- Base https://www.html.it/guide/guida-css-di-base/
- CSS3 https://www.html.it/guide/guida-css3/
- Tecniche https://www.html.it/guide/guida-css-tecniche-essenziali/
## Esercizio

Creare la pagina **login.html** che implementi una form di accesso ad un ipotetico servizio che separi in modo netto la struttura del documento dallo stile di visualizzazione.

Usare la pagina **login-esempio.html** come possibile punto di partenza.

