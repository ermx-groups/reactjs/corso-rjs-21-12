1. Cos'è ReactJS
Introduzione
Perchè utilizzare ReactJS
Vantaggi e Limitazioni

2. Introduzione a JSX
Virtual DOM
JS contro JSX
Componenti di ReactJS
Attributi HTML
Child Component e Namespaced Component
Expression Javascript in JSX

3. Configurazione e Installazione ambiente di sviluppo ReactJS
Istallazione e Setup
Utilizzo di NPM e Package.json file
Introduzione a Webpack e ES6
Organizzazione codice sorgente
ReactJS Browser Plugin

4. Creazione di una ReactJS Application
Nesting Component
React Render
React Props
Propos Validation con Data Types
States, Inizialized States, Update States

5. React UI e Forms
Form Components
Controlled Form Components
Uncontrolled Form Components
Checkboxes e Radios
Select Boxe con select value default
Form Validation
Styles
-- Animazione [non ancora]

6. Ciclo di Vita delle Componenti
Inizial Render
Propos Change
Stage Change
Component Unmounth

8. Gestione degli Eventi in JSX
Panoramica Eventi
onClick, onKeyUp
Gestione degli Eventi attraverso le componenti

9. React Styles
CSS e Inline Styles
-- Configurazione React per React Bootstrap
-- Implementazione React Bootstrap Components

12. Redux
Store
Provider Component
Actions
Reducers
Sviluppo applicazione Redux

7. React Router
Configurazione React Router
Router History
If-els in JSX
IIFE in JSX

10. React Router
Router Library
Configurazione Router
Passaggio e Ricezione Parametri

11. Lint StyleLint e Flow
Introdurre eslint nel progetto
Introduzione a Flow e lo static type checker;
Utilizzare flow con progetto di esempio

13. Unit Testing
Tool per la fase di Unit Testing
React Unit Testing
JEST
React Component Testing
React Router Testing