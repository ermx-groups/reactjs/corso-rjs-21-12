import dati from '../data/listaProdotti.json';

function getListaProdottiLocale() {
    return dati;
}


async function getListaProdottiRemoti() {
    let result = await fetch('http://pastabarilla.it/getDAtiProd');
    let dati = await result.json()
    return dati;
}

export default getListaProdottiLocale;