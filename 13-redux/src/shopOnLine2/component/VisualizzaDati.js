import DettaglioProdotto from "./DettaglioProdotto"

export default  ({dati}) => {
    return dati.map(item => <DettaglioProdotto key={item.id} info={item}/>)
}