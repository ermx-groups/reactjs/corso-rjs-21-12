import dati from '../data/listaProdotti.json';
import getListaProdotti from '../lib/getListaProdotti';
import VisualizzaDati from './VisualizzaDati';

export default () => {
    let dati = getListaProdotti();
    
    return <div>
        <h2>Lista Prodotti</h2>
        <VisualizzaDati dati={dati}/>
    </div>
}