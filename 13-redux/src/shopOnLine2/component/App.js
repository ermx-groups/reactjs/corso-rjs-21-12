import { Route, Routes } from "react-router-dom"
import MenuNavigazione from "./MenuNavigazione"
import VistaCarrello from "./VistaCarrello"
import VistaListaProdotti from "./VistaListaProdotti"

export default () => {
    return <>
        <MenuNavigazione/>
        <Routes>
            <Route path="" element={<VistaListaProdotti/>}/>
            <Route path="carrello" element={<VistaCarrello/>}/>
        </Routes>
    </>
}