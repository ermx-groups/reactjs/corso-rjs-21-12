import { useState } from "react";
import { useDispatch } from "react-redux";
import Quantita from "../Quantita";
import "./DettaglioProdotto.css";

export default ({info}) => {
    let [qta, setQta] = useState(1);
    const dispatch = useDispatch();
    const onAcquista = () => {
        console.log({type:"aggiungiProdotto", payload : {id: info.id, qta: qta}});
    }
    const setQuantita = (val) => {
        setQta(val);
    }
    return <div className="prodotto">
        <div>{info.descr}</div>
        <div>{info.prezzo} &euro;</div>
        <div><span>Max</span> <code>{info.giac}</code></div>
        <div><Quantita min={1} max={info.giac} value={qta} onQuantita={setQuantita}/></div>
        <div style={{padding:"3px 5px"}}><button onClick={onAcquista}>Acquista</button></div>
    </div>
}