import { Link } from "react-router-dom"

export default () => {
    return <nav>
        <Link to="">Prodotti</Link> | 
        <Link to="carrello">Carrello</Link>
    </nav>
}