
export default ({min, max, value, onQuantita}) => {
    function checkData(e) {
        let val = parseInt(e.target.value);
        if (isNaN(val)) {
            val = '';
        } else {
            if (val < min) {
                val = min;
            }
            if (val > max) {
                val = max;
            }
        }
        onQuantita(val);
    }
    return <>
        <button>-</button>
        <input value={value} style={{width:"50px", textAlign:"right"}} onChange={checkData}/>
        <button>+</button>
    </>
}