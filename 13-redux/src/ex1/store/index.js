import {createStore, combineReducers, applyMiddleware} from 'redux';
import chart from '../ex1/reducer/chart';
import user from '../ex1/reducer/user';
import products, { middlewareProducts } from '../ex1/reducer/products';

const store  = createStore(combineReducers({
    chart, user, products
}), applyMiddleware(middlewareProducts));

export default store;