import { useDispatch, useSelector } from "react-redux"
import { actionUserLogin, actionUserLogout } from "../reducer/user";

export default () => {
    let logged = useSelector(state => state.user);
    let dispatch = useDispatch();
    console.log('User');
    return <div>
        {logged ? 
            <button type="button" onClick={()=>dispatch(actionUserLogout())}>Esci</button> :
            <button type="button" onClick={()=>dispatch(actionUserLogin({id:1, nome:"Ermanno"}))}>Accedi</button>
        }
    </div>
}