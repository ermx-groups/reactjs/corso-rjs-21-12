import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { actionProcutsGetData } from "../../reducer/products";
import Product from "./Product";

export default () => {
    let dispatch = useDispatch();
    useEffect(()=>dispatch(actionProcutsGetData()), []);
    let data = useSelector(state => state.products.data);
    return <div>
        <h1>Prodotti</h1>
        {data.map(item => <Product key={item.id} data={item}/>)}
    </div>
}