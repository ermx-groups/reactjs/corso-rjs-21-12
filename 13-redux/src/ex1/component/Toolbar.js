import Chart from "./Chart";
import User from "./User";
import { Link, Outlet} from "react-router-dom";

export default () => {
    console.log('Toolbar');
    return <div className="toolbar">
        <nav>
            <Link to={"/"}>Products</Link> | {" "}
            <Link to="/chart">Chart</Link>
        </nav>
        <Chart/>
        <User/>
        <Outlet/>
    </div>
}