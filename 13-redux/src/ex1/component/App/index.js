import './App.css';
import Toolbar from '../Toolbar';
import ProductList from '../ProductList';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { actionProcutsGetData } from '../../../reducer/products';
import {Routes, Route} from "react-router-dom";
import ChartDetail from '../ChartDetail';
function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="/" element={<Toolbar/>}>
                <Route path="" element={<ProductList/>}/>
                <Route path="chart" element={<ChartDetail/>}/>
            </Route>
        </Routes>
    </div>
  );
}

export default App; 