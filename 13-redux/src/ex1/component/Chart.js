import { useSelector } from "react-redux"

export default () => {
    console.log('Chart');
    let [pezzi, totale] = useSelector(state => [state.chart.pezzi, state.chart.totale]);
    return <div>
        <div>Pezzi : {pezzi} </div>
        <div>Totale : {totale} </div>
    </div>
}