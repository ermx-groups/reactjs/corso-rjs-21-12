const SET_DATA = 'product/set-data';
const GET_DATA = 'product/get-data';

export default (state = {data : [], loading : false}, action) => {
    switch(action.type) {
        case SET_DATA :
            return {data : action.payload, loading : false};
        case GET_DATA : {
            return {...state, loading : true}
        }
        default :
            return state ;
    }
}

function actionProcutsGetData() {
    return {
        type : GET_DATA
    }
}

function actionProcutsSetData(data) {
    return {
        type : SET_DATA,
        payload : data
    }
}
const middlewareProducts = (store) => (next) => (action) => {
    if (action.type === GET_DATA) {
        console.log('richiesta dati', action);
        setTimeout(()=>fetch('/products.json')
                    .then(result=>result.json())
                    .then(data => store.dispatch(actionProcutsSetData(data))), 3000);
    } else {
        next(action);
    }
}

export {actionProcutsGetData, middlewareProducts}