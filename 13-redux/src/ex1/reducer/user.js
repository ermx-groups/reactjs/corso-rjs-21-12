const USER_LOGIN = 'user-login';
const USER_LOGOUT = 'user-logout';

export default (state = null, action) => {
    switch(action.type) {
        case USER_LOGIN :
            return action.payload;
        case USER_LOGOUT : 
            return null;
        default : 
            return state;
    }
}

function actionUserLogin(user) {
    return {
        type : USER_LOGIN,
        payload : user
    }
}

function actionUserLogout() {
    return {
        type : USER_LOGOUT
    }
}

export {actionUserLogin, actionUserLogout};