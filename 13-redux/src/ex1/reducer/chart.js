const ITEM_ADD = 'chart/add';
const ITEM_DEL = 'chart/del';

function makeTotale(ns) {
    let tots = ns.list.reduce((item, tots) => {
        tots[0] += item.count;
        tots[1] += item.count * item.price;
        return tots;
    }, [0, 0]);
    ns.pezzi = tots[0];
    ns.totale = tots[1];
    return ns;
}

export default (state = {list:[], totale : 0, pezzi : 0}, action) => {
    switch(action.stype) {
        case ITEM_ADD: {
            let ns = {...state, list : [...state.list, action.payload]};
            return makeTotale(ns);
        }
            return [...state, action.payload];
        case ITEM_DEL:
            let ns = {...state, list : state.list.filter(item => item.id !== action.payload)};
            return makeTotale(ns);
        default:
            return state;
    }
}

function actionChartAdd(item) {
    return {
        type : ITEM_ADD,
        payload : item
    }
}

function actionChartDel(id) {
    return {
        type : ITEM_DEL,
        payload : id
    }
}

export {actionChartAdd, actionChartDel}