import { useSelector } from "react-redux";

const ADD_PRODUCT = 'product/add';
const DEL_PRODUCT = 'product/del';

export default (state = [], action) => {
    switch(action.type) {
        case ADD_PRODUCT :
            let addPrd = action.payload;

            let found = false;
            let s = state.map(item => {
                let nextItem = {...item};
                if (item.id === addPrd.id) {
                    found = true;
                    nextItem.qta += addPrd.qta;
                }
                return nextItem;
            });
            if (!found) {
                s.push(addPrd);
            }
            return s;
        case DEL_PRODUCT:
            return [...state].filter(item => item.id!== action.payload);
    }
    return state;
}

function actionProductAdd(id, descr, prezzo, qta) {
    return {
        type : ADD_PRODUCT,
        payload : {id, descr, prezzo, qta : parseInt(qta)}
    }
}

function actionProductDelete(id) {
    return {
        type : DEL_PRODUCT,
        payload : id
    }
}
export {actionProductAdd, actionProductDelete};