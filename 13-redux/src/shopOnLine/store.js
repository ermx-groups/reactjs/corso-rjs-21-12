import { combineReducers, createStore } from "redux";
import chart from "./reducer/chart";

const store = createStore(combineReducers({chart:  chart}));
store.subscribe(()=>console.log('stato attuale', store.getState().chart));

export default store;