import { useSelector } from "react-redux"
import CarrelloItem from "../componenti/CarrelloItem";
export default () => {
    let prds = useSelector(state => state.chart);
    console.log(prds);
    let tot = 0;
    return <div>
        {prds.map(item=>{
            tot += item.prezzo*item.qta;
            return <CarrelloItem key={item.id} item={item}/>
        })}
        <div>Totale : {tot}</div>
    </div>
}