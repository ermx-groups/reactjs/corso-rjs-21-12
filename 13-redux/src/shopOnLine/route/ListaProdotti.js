import getData from '../listaProdotti';
import Prodotto from '../componenti/Prodotto';
import './ListaProdotti.scss';

export default () => {
    let dati = getData();
    return <div className="lista-prodotti">
        <h1>Lista prodotti</h1>
        <div className="lista">
            {dati.map(item=> <Prodotto key={item.id} item={item}/>)}
        </div>
    </div>
}