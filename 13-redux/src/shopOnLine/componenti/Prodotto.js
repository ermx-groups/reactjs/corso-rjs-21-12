import { useState } from "react"
import { useDispatch, useSelector } from "react-redux";
import {actionProductAdd} from "../reducer/chart";

export default ({item}) => {
    let [qta, setQta] = useState(1);
    const verificaQta = (event) => {
        let input = event.target;
        if (input.value <= item.giac) {
            setQta(input.value);
        }
    }
    const aggiungiProdotto = () =>
    {
        dispatch(actionProductAdd(item.id, item.descr, item.prezzo, qta));
        setQta(1);
    }
    
    let dispatch = useDispatch();
    let prdChart = useSelector(state => state.chart.find(prd => prd.id === item.id));
    let qtaChart = 0;
    if (prdChart) {
        console.log(prdChart);
        qtaChart = prdChart.qta;
    }

    return <div className="prodotto">
        <div>
            <div className="descr">{item.descr}</div>
            <div className="giac">max : {item.giac - qtaChart} </div>
            <input type="text" value={qta} size="2" onChange={verificaQta}/>
            <div className="prezzo"> prezzo : <span>{item.prezzo}</span></div>
            <div className="totale"> totale : <span>{item.prezzo * qta}</span></div>
            {item.giac > qtaChart &&
                <button type="button" onClick={aggiungiProdotto}>Acquista</button>
            }
        </div>
    </div>
}