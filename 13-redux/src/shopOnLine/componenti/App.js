
import ListaProdotti from "../route/ListaProdotti";
import CarrelloDettaglio from "../route/CarrelloDettaglio";
import Toolbar from "./Toolbar";
import { Routes, Route } from "react-router-dom";
import Page from "./Page";
/*
export default () => {
    return <div>
        <Routes>
            <Route path="/" element={<Page/>}>
                <Route path="/" element={<ListaProdotti/>}/>
                <Route path="/chart" element={<CarrelloDettaglio/>}/>
            </Route>
        </Routes>
    </div>
}
*/
export default () => {
    return <div>
        <Routes>
            <Route path="/" element={<Page/>}>
                <Route path="" element={<ListaProdotti/>}/>
                <Route path="chart" element={<CarrelloDettaglio/>}/>
            </Route>
            <Route path="/error" element={<div>Errore</div>}/>
        </Routes>
    </div>
}