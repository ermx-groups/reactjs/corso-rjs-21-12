import { useSelector } from "react-redux"

export default () => {
    let prds = useSelector(state => state.chart);
    console.log(prds);
    return <div style={{float:"right", padding:"2px 5px"}}>
        <b>{prds.reduce((tot, item) => item.prezzo * item.qta + tot, 0)}</b>
    </div>
}