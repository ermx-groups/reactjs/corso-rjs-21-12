import { Link, Outlet } from "react-router-dom"
import Toolbar from "./Toolbar";

export default () => {
    return <div>
        <Toolbar/>
        <hr/>
        <Outlet/>
    </div>
}