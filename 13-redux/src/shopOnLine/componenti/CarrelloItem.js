import { useDispatch } from "react-redux"
import { actionProductDelete } from "../reducer/chart";

export default ({item}) => {
    let dispatch = useDispatch();
    return <div>
        <img src={item.fileImage}/> 
        <div>{item.descr} : {item.prezzo} * {item.qta}</div> 
        <div> {item.qta * item.prezzo} <button onClick={()=>dispatch(actionProductDelete(item.id))}>X</button></div>
    </div>
}