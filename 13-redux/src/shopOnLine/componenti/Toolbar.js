
import { Link } from "react-router-dom";
import ChartWidget
 from "./ChartWidget";
export default () => {
    return <nav>
        <Link to="/">Prodotti</Link> | {" "}
        <Link to="/chart">Carrello</Link>
        <ChartWidget/>
    </nav>
}