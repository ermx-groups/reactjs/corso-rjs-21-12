### Esercitazione da fare a casa
Creare un negozio on-line che permetta:
- acquistare i beni proposti specificandone la quantità (che deve andare da un minimo di 1 ad un massimo di quanto ce n'è in magazzino)
- gli acquisti vanno messi in un carrello
- il totale del carrello è sempre visibile inisieme al totale dei pezzi acquistati
- Si può visualizzare il dettaglio del carrello per modificarne la quantità dei prodotti acquistati o addirittura toglierli
- possibilità di passare dalla lista prodotti al carrello e viceversa
- un prodotto dalla lista prodotti può solo essere acquistato.
- quando riacquisto un prodotto già presente nel carrello, nel carrello non viene aggiunta una riga ma viene aumentata la quantità del relativo prodotto.
- la quantità massima di un prodotto è datto dalla giacenza meno la quantità già presente nel carrello
- quanto la quantità massima arriva a zero allora il tasto "acquista" sparisce.