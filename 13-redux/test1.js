const redux = require('redux');

const initialState = {
    user : {
        name:'Ermanno'
    }
}
// REDUCER
const reducer = (state = initialState, action) => {
    return state;
}

// STORE
const store = redux.createStore(reducer);

console.log(store.getState());