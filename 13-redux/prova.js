const redux = require('redux');

// node prova.js

// STATE

// ACTIONS
const addAction = (n) => {
    return {
        type : 'aggiungi',
        payload : {value : n},
    }
}
const subAction = (n) => {
    return {
        type : 'diminuisci',
        payload : {value : n},
    }
}

const userLogin = (u) => {
    return {
        type:'user_login',
        payload : u
    }
}
const userLogout = () => {
    return {
        type: 'user_logout'
    }
}

function setbgColor(color) {
    return {
        type:'set-background', 
        payload : color
    }
}

function setFont(font) {
    return {
        type:'set-font',
        payload: font
    }
}
// REDUCER
const reducerCounter = (state = 0, action) => {
    switch(action.type) {
        case 'aggiungi' : 
            return state + action.payload.value;
        case 'diminuisci' :
            return state - action.payload.value;
        default:
            return state;
    }
}

const reducerUser = (state = null, action) => {
    switch(action.type) {
        case 'user_login' :
            return action.payload;
        case 'user_logout' : 
            return null;
        default:
            return state;
    }
}

const reducerScreen = (state = {bgcolor:"red", font:"verdana"}, action) => {
    switch(action.type) {
        case 'set-background':
            return {bgcolor : action.payload}
        default:
            return state;
    }
}

let rootReducer = redux.combineReducers({
    counter: reducerCounter,
    user : reducerUser,
    screen : reducerScreen 
})
// STORE

const logger = (store) => (next) => (action) => {
    let state = store.getState();

    console.log('pre', store.getState(), action);
    if (action.type=='aggiungi') {
        if (state.user === null) {
            action.payload.value = action.payload.value>10 ? 10 : action.payload.value;
        }
    }
    if (action.type ==='set-font') {
        let state = store.getState();
        if(state.screen.bgcolor === 'yellow') {
            return;
        }
    }
    next(action);
}

const store = redux.createStore(rootReducer, {}, redux.applyMiddleware(logger));

store.subscribe(()=>console.log('post', store.getState()));

/*
store.dispatch(addAction(15));
store.dispatch(userLogin({id:1, nome:"Ermanno"}));
store.dispatch(addAction(15));
store.dispatch(userLogout());
store.dispatch(addAction(15));
*/
store.dispatch(setbgColor('black'));
store.dispatch(setbgColor("red"));

store.dispatch(setbgColor('yellow'));
store.dispatch(setFont('courier'));
store.dispatch(setbgColor('white'));
store.dispatch(setFont('courier'));

//store.dispatch(....) imposta il bgcolor a yellow
// ... imposta il font a courier -> il font non viene modificato
// ... imposta il bgcolor a white
// ... imposta il font a courier -> il font viene modificato
