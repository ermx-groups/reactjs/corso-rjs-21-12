

function Table(props) {
    var list = props.dati;
    return React.createElement(
        "table",
        null,
        React.createElement(
            "thead",
            null,
            React.createElement(
                "tr",
                null,
                React.createElement(
                    "th",
                    null,
                    "Nome"
                ),
                React.createElement(
                    "th",
                    null,
                    "Cognome"
                ),
                React.createElement(
                    "th",
                    null,
                    "Et\xE0"
                )
            )
        ),
        React.createElement(
            "tbody",
            null,
            list.map(function (rec) {
                return React.createElement(RowData, { dato: rec });
            })
        )
    );
}

function RowData(props) {
    var aa = props.dato;
    return React.createElement(
        "tr",
        null,
        React.createElement(
            "td",
            null,
            aa.nome
        ),
        React.createElement(
            "td",
            null,
            aa.cognome
        ),
        React.createElement(
            "td",
            null,
            aa.eta
        )
    );
}

var lista = [{ nome: "Ermanno", cognome: "Astolfi", eta: "??" }, { nome: "Mario", cognome: "Draghi", eta: "??" }, { nome: "Pape", cognome: "Rino", eta: "??" }, { nome: "Carlo", cognome: "Carletti", eta: "??" }];

ReactDOM.render(React.createElement(Table, { dati: lista }), document.getElementById('app'));