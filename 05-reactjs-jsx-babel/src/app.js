

function Table(props) {
    let list = props.dati;
    return <table>
        <thead>
        <tr>
            <th>Nome</th><th>Cognome</th><th>Età</th>
        </tr>
        </thead>
        <tbody>
            {list.map((rec) => <RowData dato={rec}/>)}
        </tbody>
    </table>
}

function RowData(props) {
    let aa = props.dato;
    return <tr>
        <td>{aa.nome}</td>
        <td>{aa.cognome}</td>
        <td>{aa.eta}</td>
    </tr>
}

let lista = [
    {nome:"Ermanno", cognome:"Astolfi", eta:"??"},
    {nome:"Mario", cognome:"Draghi", eta:"??"},
    {nome:"Pape", cognome:"Rino", eta:"??"},
    {nome:"Carlo", cognome:"Carletti", eta:"??"}
];

ReactDOM.render(
    <Table dati={lista}/>,
    document.getElementById('app')
);
