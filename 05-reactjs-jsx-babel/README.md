# Utilizzo del preprocessore BABEL

<blockquote>https://it.reactjs.org/docs/add-react-to-a-website.html</blockquote>

Finoa ad ora abbiamo implementato elementi jsx direttamente dentro il documento html che includeva sia la libreira react/reactDOM e sia della libreria Babel.
Quest'ultima ha lo scopo di estrarre il codice JSX dai tag script di tipo "text/babel" per poterlo trasformare in oggetti JS standard.
Questa trasformazione avviene nel client .. quindi è il client che si sobbarca il lavoro di traduzione (di seguito transpiling .. effettuate da un transpiler) e di successiva renderizzazione.

Una ottimizzazione ovvia è quella di togliere al client l'incombenza del transpiling.
Quindi l'idea è quella di generare sul server il file tradotto e di fornirlo direttamente .. così da far concentrare il client al solo rendering.

Per fare questo occorre:
- installare node
- installare la libreria babel-cli e babel-preset
```console
$ npx npm init -y
$ npm install babel-cli@6 babel-preset-react-app@3
```
Il lavoro del translitter ora è quello di prendere un file sorgente in jsx e generare un file destinazione in js. Quindi, se ipotizziamo che i nostri file jsx siano in una directory di "lavoro", sarebbe comodo se il traslitter generasse i file tradotti con lo stesso nome, ma in una directory di "produzione".
Questo lavoro viene fatto da questo comando node
```console
npx babel --watch src --out-dir dest --presets react-app/prod 
```
Questo comando avvia un wacher (quindi non termina) che osserva costantemente la directory <code>src</code> al fine di generare in <code>dest</code>. 
Ogni modifica ad un file in src genera una ricompilazione sul file in dest.