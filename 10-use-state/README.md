## Esercizi

1. Aggiungere al componente della form dell'agenda un input controllato che gestisca il formato data
1. Cominciare a studiare il progetto Tic-Tac-Toe (tris) presente sul tutorial reactjs.it. Potete iniziare da qui (https://it.reactjs.org/tutorial/tutorial.html#overview)

## Form dinamica
Costruire una form di prenotazione per un albergo che abbia i seguenti campi :
1. numero adulti
1. numero bambini
1. lista di input per la richiesta dell'età dei bimbi

Tutti gli input possono contenere solo numeri interi e il valore non può essere modificato direttamente ma solo attraverso i tasti [-] e [+] che tolgono o aggiungono 1 al valore già presenete nell'input.

Il risultato visivo sarà quindi  ...

> Adulti : [-] 3 [+]
Bambini : [-] 0 [+]

Oppure

>Adulti : [-] 2 [+]
Bambini : [-] 2 [+]
Età bambino 1 : [-] 5 [+]
Età bambino 2 : [-] 12 [+]

Al variare del numero di "Bambini" occorre adeguare le righe di "Età bambinio n".

I valori che possono assumere gli input di "Adulti" e "Bambini" sono da 0 a 5.
I valori che possono assumeri gli input delle Età sono da 0 a 17.

Realizzare i componenti con stato necessari sia in versione __class__ che in versione __hook__.

