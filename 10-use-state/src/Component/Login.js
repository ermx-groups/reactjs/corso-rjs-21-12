import React, {useState} from "react";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login : '',
            password : '',
            timeout : 20
        }
    }
    componentDidMount() {
        this.timer = setInterval(()=>this.setState(state => {if (state.timeout) return {timeout: state.timeout-1}}), 1000);
        /*
        fetch('http://localhost:3000/data.json', {method:'GET'})
        .then(response => response.json())
        .then(console.log);
        */
    }
    componentDidUpdate() {
        console.log('update');
    }
    componentWillUnmount() {
        
        clearInterval(this.timer);
    }
    render() {
        return <>
            <h1>Effettua il login</h1> {
                true ? <>
                    <input name="login" placeholder="Login" type="text" value={this.state.login} onChange={(e)=>this.setState({login:e.target.value})}/><br/>
                    <input name="password" placeholder="Password" type="password" value={this.state.password}  onChange={(e)=>this.setState({password:e.target.value})}/><br/>
                    <button type="button" onClick={() => this.props.onLogin(this.state.login, this.state.password)}>Accedi</button>
                </> : <div>Tempo scadudo</div>
            }
        </>  
    }
}

function LoginHook (props) {
    let arr = [1,2,3];
    let [primoValue, secondoValore] = arr;
 
    let [login, setLogin] = useState('');
    let [password, setPassword] = useState('');
    
    return <>
            <h1>Effettua il login</h1>
            <input name="login" placeholder="Login" type="text" value={login} onChange={(e)=>setLogin(e.target.value)}/><br/>
            <input name="password" placeholder="Password" type="password" value={password}  onChange={(e)=>setPassword(e.target.value)}/><br/>
            <button type="button" onClick={() => this.props.onLogin(login, password)}>Accedi</button>
        </>  
}
export default Login;