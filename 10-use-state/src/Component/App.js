import React, {useEffect, useState} from 'react';
import './App.css';
import ViewData from './ViewData';
import Login from './Login';

/*
const AppState = (props) => {
  let [user, setUser] = useState(null);

  const handlerLogin = () => {
    setUser(true);
  }
  const handlerLogout = () => {
    setUser(null);
  }
  useEffect(()=>{
    console.log('+++');
  }, [user]);
  console.log('...');
  return (
    <div className="App">
      {
        user === null ? 
          <Login onLogin={handlerLogin}/> : 
          <ViewData onLogout={handlerLogout}/>
      }
    </div>
  );
}
*/
const getData = () => {
  return [
    {login:'admin', password:'admin1'},
    {login:'guest', password:'1234'},
    {login: 'erm', password:"marco"}
  ]
}
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {user : null, cc : 0}
  }

  handlerLogin(login, password) {
    let userList = getData();
    let userAct = userList.find(user => user.login === login && user.password === password);
    
    if (userAct == null) {
      this.setState({user : null, error : 'Utente non riconosciuto'});
    } else {
      this.setState({user: userAct, error:''});
    }
  }

  handlerLogout() {
    this.setState({user:null});
  }
  tick() {
    this.setState(state => ({cc : state.cc + 1}))
  }
  viewError() {
    return <>
      <h3>Errore</h3>
      <b>{this.state.error}</b>
    </>
  }
  render() {

    return (
      <div className="App">
        
        {
          this.state.user == null ? 
            <Login onLogin={(login, password)=>this.handlerLogin(login, password)}/> : 
            <>
              <ViewData user={this.state.user} onLogout={()=>this.handlerLogout()}/>
            </>
        }
        {this.state.error && this.viewError()}
      </div>
    );
  }
}

export default App;
