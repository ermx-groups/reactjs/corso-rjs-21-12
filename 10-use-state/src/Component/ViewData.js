import React from 'react';

class ViewData extends React.Component {

    constructor(props) {
      super(props);
      this.changeMessage = this.changeMessage.bind(this);
      this.state = {
        date: "", 
        message : "",
        sso:"f",
        ck_pane : true
      }
    }

    checkDate(tmp) {
      let date = tmp.date;
      let part = date.split('/');
      
      if (date == "") return true;
      let gg = parseInt(part[0]);
      let mm = parseInt(part[1]);
      if (part.length>= 1) {
        if (part[0].length < 2 && part.length<2) return true;
        if (gg<1) return false;
      }
      if (part.length>=2) {
        if (part[1].length < 2 && part.length<3) return true;
        if (mm<1) return false;
      }
      return true;
    }

    changeMessage(value) {
      let re = new RegExp("^[0-3]?([0-9](\/([0-1]?[0-9](\/[0-9]{0,4})?)?)?)?$");
      if(!re.test(value)) {
        return;
      }
      if (this.state.date < value) {
        if (value.length === 2 || value.length === 5) {
          if (value[value.length-1] !='/') {
            let newValue = value + '/';
            if (re.test(newValue)) {
              value = newValue;
            }
          }
        }
      }
      let tmp = {
        date : value
      }
      if (value.length <= 10 && this.checkDate(tmp)) {
        this.setState({date: tmp.date});
      }
    }

    componentDidMount() {
      this.timer = setInterval(()=>{}, 10000);
    }
    componentDidUpdate() {
      //
    }
    componentWillUnmount() {
      //
      clearInterval(this.timer);
    }
    
    render() {
      return <>
        <div>
          <div>
            <input placeholder='Data dd/mm/yyyy' type="text" value={this.state.date} onChange={(e) => this.changeMessage(e.target.value)}/>
          </div>
          <div>
            <label>Seleziona</label><br/>
            <select name="sso" value={this.state.sso} onChange={(e)=>this.setState({sso: e.target.value})}>
              <option value="">--seleziona--</option>
              <option value="m">Maschio</option>
              <option value="f">Femmina</option>
            </select>
          </div>
          <div>
            <label>Testo</label><br/>
            <textarea rows="5" value={this.state.message} onChange={(e)=>this.setState({message:e.target.value})}></textarea>
          </div>
          <div style={{textAlign:"left"}}>
            <label>Spesa</label><br/>
            <input type="checkbox" name="ck_pane" value="1" checked={this.state.ck_pane} onChange={(e)=>this.setState({ck_pane:e.target.checked})}/> Prendi la frutta<br/>
            {this.state.ck_pane && <>
              <input type="checkbox" name="ck_frutta" value="1"/> Castagne<br/>
              <input type="checkbox" name="ck_frutta" value="1"/> Mele<br/>
              <input type="checkbox" name="ck_frutta" value="1"/> Pere<br/>
            </>}
            
            <input type="checkbox" name="ck_prosc" value="1"/> Prendi il Prosciutto<br/>
          </div>
      </div>
    </>;
    }
  }

  export default ViewData;