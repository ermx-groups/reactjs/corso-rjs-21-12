import React, {useState, useEffect} from 'react';
import './App.css';

function Contatore (props) {
    return <div>
        <label>{props.label}</label>
        <div className="inputValue">
            <button type="button" className="button sub" onClick={()=>props.onChange(-1)}>-</button>
            <input type="text" name={props.name} value={props.value} onChange={()=>{}}/>
            <button type="button" className="button add" onClick={()=>props.onChange(+1)}>+</button>
        </div>
    </div>
}

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            adulti : 2,
            bambini : 3,
            eta : [5, 8]
        }
        this.handlerAdulto = this.handlerAdulto.bind(this);
        this.handlerBambino = this.handlerBambino.bind(this);
        this.handlerCampoInc = this.handlerCampoInc.bind(this);
    }

    handlerAdulto(inc)  {
        let nuovoValore = this.state.adulti + inc;

        if (nuovoValore<0) return;
        if (nuovoValore>5) return;

        this.setState({adulti :nuovoValore});
    }

    handlerBambino(inc)  {
        let nuovoValore = this.state.bambini + inc;

        if (nuovoValore<0) return;
        if (nuovoValore>7) return;

        this.setState({bambini :nuovoValore});
    }

    handlerIncrementa(campo, inc) {
        let nuovoValore = this.state[campo] + inc;
        if (nuovoValore<0) return;
        if (nuovoValore>5) return;

        this.setState({[campo] :nuovoValore}); 
        return null;      
    }

    handlerCampoInc (campo) {
        return (inc) => {
            let nuovoValore = this.state[campo] + inc;
            if (nuovoValore<0) return;
            if (nuovoValore>5) return;
            
            this.setState({[campo] :nuovoValore}); 
            return null;             
        }
    }

    render() {
        return <div>
            <div>
                <label>Adulto</label><Contatore value={this.state.adulti} onChange={this.handlerCampoInc('adulti')}/>
            </div>
            <div>
                <label>Bambini</label><Contatore value={this.state.bambini} onChange={this.handlerCampoInc('bambini')}/>
            </div>
        </div>
    }
}

function AppHook (props) {
    let [adulti, setAdulti] = useState(0);
    let [bambini, setBambini] = useState(0);
    let [eta, setEta] = useState([]);

    useEffect( () => {
        fetch('/data.json')
        .then(response => response.json())
        .then(data => {
            console.log(data);
            setAdulti(data.adulti);
            setBambini(data.bambini.length);
            setEta(data.bambini);
        });
        
       console.log([adulti, bambini]);
       return () =>{
           console.log('destruct');
       }
    }, []);

    useEffect(() => {
        let nuovaEta = [];
        for(let idx = 0; idx < bambini; idx++) {
            nuovaEta .push((eta.length > idx) ? eta[idx] : 8);
        }
        console.log('bambini', eta);
        setEta(nuovaEta);
        return () =>{
            console.log('cancel', eta);
        }
    }, [bambini]);

    const modificaAdulti = () => {
        return (inc) => {
            let nuovoValore = adulti + inc;
            if (nuovoValore<0) return;
            if (nuovoValore>5) return;
            setAdulti(nuovoValore);
        }
    }
    const modificaBambini = () => {
        return (inc) => {
            let nuovoValore = bambini + inc;
            if (nuovoValore<0) return;
            if (nuovoValore>5) return;

            setBambini(nuovoValore);
        }
    }
    const modificaEta = (idx) => (inc) => {
        let nuovaEta = [ ... eta ];
        nuovaEta[idx] += inc;
        if (nuovaEta[idx] < 0) return;
        if (nuovaEta[idx] > 17) return;
        setEta(nuovaEta);
    }
    
    const sendData = () => {
        fetch('/api/prenota', {method:'POST', body : JSON.stringify({adulti, bambini, eta})})
        .then(response=>response.json())
        .then(console.log);
    }
    return <div>
        <div>
            <Contatore label="Adulti" value={adulti} onChange={modificaAdulti()}/>
        </div>
        <div>
            <Contatore label="Bambini" value={bambini} onChange={modificaBambini()}/>
        </div>
        <hr/>
        {eta.map((val, idx) => <div key={idx}><Contatore label={"Età "+(idx+1)} value={val} onChange={modificaEta(idx)}/></div>)}
        <hr/>
        <button onClick={sendData}>Invia</button>
    </div>
}

export default AppHook;
