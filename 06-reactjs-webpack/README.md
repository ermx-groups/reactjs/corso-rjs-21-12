# Utilizzo di webpack + babel con React
Pagina di riferimento https://www.freewebsolution.it/installare-react-mediante-webpack-e-babel/

Webpack è un static module bundler .. cioè un gestore di risorse statiche.
Attraverso webpack è possibile accorpare codice javascript suddiviso in più file, processare le diverse risorse in base al loro tipo (scss in css) e renderle disponibili come link via web.

Webpack è installabile come normale libreria javascript.

L'obiettivo è quello di creare sul server tutto quello che serve al client, senza dargli l'incombenza di dover cercare altrove le risorse necessarie.

Quindi scarichiamo react
```
npm install react react-dom
```
Dopo di che installiamo babel e webpack come librerie di sviluppo
```bash
npm install --save-dev babel-core babel-loader@7 babel-preset-env babel-preset-react
npm install --save-dev webpack webpack-cli html-webpack-plugin
```
html-webpack-plugin permette di poter iniettare le dipendenze generate direttamente nel file index.html oltre che poter utilizzare dei placeholder

##### .babelrc
Successivamente impostiamo i file di configurazione per babel per dirgli di dover utilizzare il preset react nel suo lavoro di transpiler
```
{"presets":["env", "react"]}
```
##### webpack.config.js
Quindi impostiamo la configurazione di webpack
```javascript
var path    = require('path');
var hwp     = require('html-webpack-plugin');
module.exports = {
    entry: path.join(__dirname, '/src/index.js'),
    output: {
        filename: 'build.js',
        path: path.join(__dirname, '/dist')
    },
    module:{
        rules:[{
            exclude: /node_modules/,
            test: /\.js$/,
            loader: 'babel-loader'
        }]
    },
    plugins:[
        new hwp({template:path.join(__dirname, '/src/index.html')})
    ]
}
```
<code>src/index.js</code> è il punto di partenza del processo di packettizzazione.
<code>dist</code> è la directory di destinazione che contiene il file <code>build.js</code> quale risultato del processo di build.

Introduciamo così 2 nuovi comandi personalizzati in package.json (sezione script)
```json
"scripts": {
    "start": "webpack-dev-server --mode development --open --hot",
    "build": "webpack --mode production"
}
```
#### Caricamento Css
Per poter caricare i css tramite <code>import</import> occorre installare anche le seguenti librerie
```
npm install --save-dev style-loader css-loader
```
ed abilitare il loader in webpack.config.js aggiungendo il rule
```javascript
{
    test: /\.css$/,
    use: [ 'style-loader', 'css-loader' ]
}
```
<code>css-loader</code> risolverà le *url()* e gli *@import* generando una stringa css, mentre <code>style-loader</code> si preoccupa di iniettare questa stringa nel documento.
Da notare che webpack esegue i loader in ordine inverso rispetto a come vengono inseriti in *use*.

__Da studiare__ : https://www.ionos.it/digitalguide/siti-web/programmazione-del-sito-web/sass/


Infine è possibile aggiungere un pre-processore sass per poter generare il css corrispondente.
Per ottenere questo risultato occorre installare le seguenti librerie
```
npm i --save-dev sass-loader node-sass
```
ed aggiungere il loader sass alla catena di processamento dei file di stile
```javascript
    {
        test: /\.(s(a|c)ss)$/,
        use: ['style-loader','css-loader', 'sass-loader']
    }
```
Il primo loader chiamato sarà il sass-loader che genera il css standard. Il suo ouput verrà processato dai restanti nodi della catena.
