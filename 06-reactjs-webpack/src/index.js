import React from 'react';
import ReactDOM from 'react-dom';
// import './css/base.scss';

const Descr = (props) => {
    return <div className="descr">{props.testo}</div>
}
const Box =(props)=> {
    return <div className="box">
<h2>{props.titolo}</h2>
<Descr testo = {props.corpo}/>

    </div>
}


const App = () =>(
    <div className="app">
        <h1>Prodotti nuovi in listino</h1>
        <div className="descr">Nuovi prodotti 2022</div>
      <Box titolo="Andromeda" corpo="Applicazioni web"/>
      <Box titolo="Marte" corpo="Applicazioni desktop"/>
      
        </div>
   
)

ReactDOM.render(
    <App/>,
    document.getElementById('root')
)