import React from "react";

export default class HH extends React.Component {
    constructor(prop) {
        super(prop);
        this.handlerClick = this.handlerClick.bind(this);
    }
    handlerClick() {
        console.log(this);
    }
    render() {
        return <h1 className="blue" onClick={this.handlerClick}>{this.props.text}</h1>
    }
}

export function Non(props) {
    return <div>hello</div>;
}
export function Abla(pros) {
    return <div>ciao</div>;
}