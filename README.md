# corso-rjs-21-12

Corso React.js iniziato a dicembre 2021

## Strumenti di sviluppo
- Visual studio code
- git
- account su gitlab.com
- nodejs

## Note sullo sviluppo
Gitlab verrà utilizzato come gestore del codice sviluppato.
Il branch **main** è riservato al docente. In questo ramo verranno inseriti tutti i contenuti necessari al corso, compreso le esercitazioni.

Ogni studente dovrà creare il proprio branch cognome-nome sul quale potrà effettuare tutte le modifiche che riterrà opportuno. Per consegnare il proprio lavoro basterà pushare il proprio ramo (una volta che sono state fatte tutte le commit locali).

Per poter recuperare le modifiche effettuate sul ramo principale (tipicamente le aggiunte fatte dal docente) occorre effettuare i seguenti passaggi :
- assicurarsi di essere sul proprio ramo di sviluppo
- ``git fetch`` *aggiornamento codice locale da gitlab*
- ``git merge main``
- non modificare mai i file del ramo principale (main), ma fare solo aggiunte. I file aggiunti possono essere modificati sempre.

## Aggiornamenti

- Definizione prime lezioni con materiale di studio