import React from 'react';
import './ListaElementi.css';

class ListaElementi extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        lista : props.lista
      }
      this.props.in.send = (item) => {
        let ln = [...this.state.lista];
        ln.push(item);
        this.setState({lista: ln});
      }
      this.found = null;
    }
    updateData(idDaCacellare) {
        let ll = this.state.lista;
        let ln = ll.filter(item=>{
          if (item.id == idDaCacellare) this.found = item; 
          return item.id != idDaCacellare;
        });
        this.setState({lista : ln});
        this.props.out.send(this.found);
    }
    render () {
      return <div className="lista">
        {this.state.lista.map(item => <div key={item.id} onClick={()=>this.updateData(item.id)}>{item.descr}</div>)}
      </div>
    }
  }

  export default ListaElementi;