import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AppReact from './App';


ReactDOM.render(
  <>
    <AppReact />
  </>,
  document.getElementById('root')
);