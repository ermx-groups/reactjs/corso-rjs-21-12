import logo from './logo.svg';
import './App.css';
import React from 'react';
import Lista from './ListaElementi';

let lista1 = [{id:'1', descr:"primo elemento"}, {id:'2', descr:"secondo elemento"}];
let lista2 = [{id:'3', descr:'elemento tre'},{id:'4', descr:'elemento quattro'},{id:'5', descr:'elemento cinque'}];

/*
 |                  |  elemento 3      |
 | secodno elemento |  primo elemento  |
*/


function App() {
  let ch1 = {
    send : ()=>{}
  }
  let ch2 = {
    send : ()=>{}
  }
  const chnData = (item) => {
    alert(item.descr);
  }
  return (
    <div className="App">
      <Lista lista={lista1} in={ch1} out={ch2} onChnData={chnData}/>
      <Lista lista={lista2} in={ch2} out={ch1} onChnData={chnData}/>
    </div>
  );
}

export default App;
