## Esercizio 1

Scrivere una App che abbia 4 Liste con elementi che possono spostarsi in modo circolare, cioè :
Se premo un elemento dalla prima lista va nella seconda;
Se premo un elemento dalla seconda lista va nella terza;
Se premo un elemento dalla terza va nella quarta;
Se premo un elemento dalla quarta va nella prima;

N.B: Meglio se tra tutte le liste non ci siano elementi con "id" uguali. Questo per evitare di trovarvi ad inserire un elemento in una lista che abbia al suo interno un elemento con lo stesso id. Avreste un comportamento strano nel caso vorreste successivamente cancellarlo.