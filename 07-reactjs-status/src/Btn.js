import React from 'react';

class Btn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            testo : 'Hello'
        };
        this.modifica = this.modifica.bind(this);
    }

    modifica() {
        console.log(this.state);
        if (this.state.testo == 'Hello') {
            this.setState({testo: "World"});
        } else {
            this.setState({testo: "Hello"});
        }
    }

    render() {
        return <button type="button" onClick={this.modifica}>{this.state.testo}</button>
    }
}

export default Btn;