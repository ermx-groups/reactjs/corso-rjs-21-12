import React from "react";
import './Box.scss';

function Title (props) {
    return <h3 className="title">{props.children}</h3>;
}

function Body(props) {
    return <div className="body">{props.children}</div>;
}

function Foot (props) {
    return <div className="foot">{props.children}</div>
}

function Button(props) {
    return <button type="button" onClick={props.handler}>
        {props.children}
    </button>
}

class Box extends React.Component {

    render () {
        return <div className="box">
            <Title>{this.props.title} : {this.props.id}</Title>
            <Body>{this.props.body}</Body>
            <Foot><Button handler={this.props.handlerSelect}>Click</Button></Foot>
        </div>
    }
}

export default Box;

export {Title, Body}