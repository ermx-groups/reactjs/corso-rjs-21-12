import React, {Component} from 'react';
import Scatola from '../Box/index';

import './App.scss';

function getData() {
    let list = [
        {title:"uno", body:"hello", id:"233"},
        {title:"due", body:"hello jdks", id:"133"},
        {title:"tre", body:"gino", id:"2153"},
        {title:"44", body:" jskldjfalkdjfa", id:"2253"},
        {title:"678", body:"gino", id:"2353"},
        {title:"quattro", body:"chiaro", id:"243"}
    ];
    return list;
}

class App extends Component {
    
    constructor(props) {
        super(props);
        this.state = { data : getData() };
    }

    handlerSelectBox(id) {
        let lista = this.state.data;
        let nuovaLista = lista.filter( item => item.id != id );

        this.setState({data : nuovaLista});
    }

    render() {
        return <>
            <div className="app">
                {
                    this.state.data.map(
                        (item, idx) =>
                        <Scatola key={item.id} {... item} handlerSelect={() => this.handlerSelectBox(item.id)}/>
                    )
                    
                }
            </div>
        </>
    }
}

export default App; 