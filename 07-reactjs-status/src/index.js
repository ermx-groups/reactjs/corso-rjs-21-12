import React from "react";
import ReactDOM from "react-dom";
import './esercizioapp.scss';




function Ricerca(props) {

    return <div>

         <input className="input" placeholder="search" /> <span>{props.numRecord}</span>

          </div>
      
}

class ListaRisultati  extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dati : props.dati
        };
    }

    cancella(idDaCancellare) {
        let listax = this.state.dati;
        let listan = listax.filter(item=> item.id!=idDaCancellare);
        this.setState( {dati: listan});
        this.props.handlerUpdateData(listan.length);
    }

    render() {
        return  <div className="lista"> 

            {this.state.dati.map(item =>
                <Item key={item.id}  
                handlerButton={()=>this.cancella(item.id)} 
                link={item.link} 
                titolo={item.titolo}/>)}
        </div>
    }
}

function Item(props) {
    let handlerButton = props.handlerButton;

    return <div className="box">
        <div>
            <cite>{props.link}</cite> <button onClick={handlerButton} type="button">Avvia {props.id}</button>
            <h1>{props.titolo}</h1>
        </div>
        <div>
            <div>{props.paragrafo}</div>
        </div>
    </div>

}


class App extends React.Component  {

    constructor(props) {
        super(props);
        this.state = {
            numElem1 : this.props.dati.length,
            numElem2 : this.props.dati.length,
            numElem3 : this.props.dati.length
        };
    }

    render() {
        return <>
            <div>
            <table>
                <tbody>
            <tr>
                    <th>List 1 : </th><td>{this.state.numElem1}</td>
                </tr>
                <tr>
                    <th>List 2 : </th><td>{this.state.numElem2}</td>
                </tr>
                <tr>
                    <th>List 3 : </th><td>{this.state.numElem3}</td>
                </tr>
                </tbody>
            </table>
            </div>
            <div className="risultati">
                <ListaRisultati dati={this.props.dati} handlerUpdateData={(numElem)=>{this.setState({numElem1 : numElem})}}/>
                <hr/>
                <ListaRisultati dati={this.props.dati} handlerUpdateData={(numElem)=>{this.setState({numElem2 : numElem})}}/>
                <hr/>
                <ListaRisultati dati={this.props.dati} handlerUpdateData={(numElem)=>{this.setState({numElem3 : numElem})}}/>
            </div>
        </>;
    }
}

let lista = [

    {id : "1234", link:"Link 1", titolo:"Titolo 1 ", paragrafo:"testo lungo 1"},
    {id : "1235", link:"Link 2", titolo:"Titolo 2 ", paragrafo:"testo lungo 2"},
    {id : "2345", link:"Link 3", titolo:"Titolo 3 ", paragrafo:"testo lungo 3"},

];


ReactDOM.render(
    <App  dati={lista} />,

    document.querySelector('#app')
    );