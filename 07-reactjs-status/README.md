### Esecizio 1

Implementare un componente Button che visualizzi "Hello" e "World" alternativamente man mano che viene cliccato.
Cioè ... prima visualizza "Hello"; se cliccato visualizza "World"; se cliccato visualizza "Hello" etc...git 

### Esercizio 2
Creare un insieme di Componenti necessari a visualizzare una lista di risultati simile alla pagina dei risultati di google.
Il componente principale "App" deve ricevere la lista dei dati da visualizzare e passarli ai relativi componenti figli

### Esercizio 3
Modificare il file index.js per elevare lo stato dal componente *ListaRisultati* al componete *App* di modo che si possa passare il numero aggiornato dei record rimasti al componente *Ricerca* man mano che vengono cancellati.

### Esercizio 4
Abbiamo implementato App in modo che la lista dei risultati da visualizzare sia gestita attraverso il suo stato.
Tale lista viene passata al componente ListaRisultati che si preoccupa di visualizzarli.
Utilizzando però più elementi ListaRisultati ci si accorge che essi visualizzeranno sempre gli stessi elementi anche se vengono cancellati da una qualsiasi lista. Questo perché tutti gli elementi ListaRisultati sono viste su dati in comune.

L'esercizio da svolgere è quello di far gestire i dati passati ad App in modo autonomo per ogni componente ListaRisultati. Quindi i dati devono essere gestiti dallo stato di ListaRisultati di modo che ogni lista che cancella un suo elemento non modifica gli elementi presenti nelle altre liste.