import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { actionDelLibro } from "../reducers/listaLibriReducer";


const ListaLibriRiga = ({dati}) => {
    const dispatch = useDispatch();
    const cancellaLibro = () => {
        dispatch(actionDelLibro(dati.id));
    }
    return <div>
        {dati.titolo} : {dati.pagine} 
        <Link to={"/book/" + dati.id}><button>i</button></Link>
        <button onClick={cancellaLibro}>x</button>    
    </div>
}

export default ListaLibriRiga;