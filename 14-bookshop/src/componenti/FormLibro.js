import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import { actionAddLibro, actionUpdateLibro } from "../reducers/listaLibriReducer";
import { Outlet, useNavigate, useParams } from "react-router-dom";

const FormLibro = (props) => {
    let dispatch = useDispatch();
    let urlParam = useParams();

    let navigate = useNavigate();
    let book = useSelector(state => state.listaLibri.find(book=>parseInt(urlParam.id) == parseInt(book.id)));


    let [titolo, setTitolo] = useState(book?.titolo || '');
    let [pag, setPag] = useState(book?.pagine || '');
    const checkIntero = (e) => {
        let nval = e.target.value;
        let cval = parseInt(nval);
        if (isNaN(cval)) cval = '';
        setPag(cval);
    }
    const salvaForm = () => {
        if (pag=== '' || pag === 0) {
            alert('Impostare il numero delle pagine');
            return;
        }
        if (book?.id) {
            dispatch(actionUpdateLibro(book.id, titolo, pag));
        } else {
            dispatch(actionAddLibro(titolo, pag));
        }
        setTitolo('');
        setPag('');
        navigate('/');
    }
    return <div>
        <h2>Dettaglio Libro</h2>
        <div>
            <label>Titolo</label>
            <input type="text" value={titolo} onChange={(e) => setTitolo(e.target.value)}/>
        </div>
        <div>
            <label>Pagine</label>
            <input type="text" value={pag} onChange={checkIntero}/>
        </div>
        <button onClick={salvaForm}>{urlParam.id ? "Modifica" : "Salva"}</button>
        <Outlet/>
    </div>
}

export default FormLibro;