import  {useSelector} from "react-redux";
import ListaLibriRiga from "./ListaLibriRiga";
import { Link } from "react-router-dom";
const ListaLibri = (props) => {

    let lista = useSelector(state => state.listaLibri);
    return <div>
        <h1>Lista libri</h1>
        {lista.map(item => <ListaLibriRiga key={item.id} dati={item}/>)}
        <Link to="/book">
            <button type="button">Aggiungi</button>
        </Link>
    </div>
}

export default ListaLibri;