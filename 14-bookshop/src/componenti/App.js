
import './App.css';
import ListaLibri from './ListaLibri';
import FormLibro from './FormLibro';
import {Routes, Route} from "react-router-dom";

function App() {
  return (
      <div className="App">
        <Routes>
          <Route path='/' element={<ListaLibri/>}/>
          <Route path='/book' element={<FormLibro/>}>
            <Route path=':id' element={<span></span>}/>
          </Route>
        </Routes>
      </div>
  );
}

export default App;
