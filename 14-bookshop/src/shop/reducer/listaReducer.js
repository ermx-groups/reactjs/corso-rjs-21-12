const ADD_PRODOTTO = 'prodotto/add';

const listaReducer = (pstate = [], action) => {
    switch(action.type) {
        case ADD_PRODOTTO:
            let d = new Date();

            const nstate = [...pstate, {...action.payload, id: d.valueOf()}];
            return nstate;
        default :
            return pstate;
    }
}

export default listaReducer;

function addProdotto (titolo, autore, pagine) {
    return {
        type : ADD_PRODOTTO,
        payload : {
            titolo, autore, pagine
        }
    }
}

export {addProdotto}