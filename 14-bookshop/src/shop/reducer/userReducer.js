const userReducer = (pstate = {}, action) => {
    switch(action.type) {
        default :
            return pstate;
    }
}

export default userReducer;