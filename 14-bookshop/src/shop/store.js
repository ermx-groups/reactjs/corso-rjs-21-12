import {createStore, combineReducers} from "redux";
import listaReducer from "./reducer/listaReducer";
import userReducer from "./reducer/userReducer";

const store = createStore(combineReducers({
    lista : listaReducer,
    user : userReducer
}));

store.subscribe(() => console.log(store.getState()));
export default store;