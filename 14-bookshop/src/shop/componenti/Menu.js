import { Link } from "react-router-dom"

export default () => {
    return <div>
        <nav>
            <Link to="">Lista prodotti</Link>  | 
            <Link to="prodotto">Nuovo Prodotto</Link>
        </nav>
    </div>
}