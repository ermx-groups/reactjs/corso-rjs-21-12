import "./ListaProdotti.scss";
import {useSelector} from "react-redux";

export default () => {

    let listaDati = useSelector(state => {
        console.log(state); 
        return state.lista;
    });
    console.log(listaDati);
    return <div className="lista-prodotti">
        <table>
            <thead>
                <tr>
                    <th>Titolo</th>
                    <th>Autore</th>
                    <th>Pagine</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {
                    listaDati.map(item => (
                        <tr>
                            <td>{item.titolo}</td>
                            <td>{item.autore}</td>
                            <td>{item.pagine}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    </div>
}