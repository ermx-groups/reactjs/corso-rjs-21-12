import {useState} from "react";
import {useDispatch} from "react-redux";
import "./FormProdotto.scss";
import { addProdotto } from "../reducer/listaReducer";
import { useNavigate } from "react-router-dom";

export default () => {
    let [titolo, setTitolo] = useState('');
    let [autore, setAutore] = useState('');
    let [pagine, setPagine] = useState('');
    let [errore, setErrore] = useState('');
    // questo hook permette di poter spostarsi tra le varie pagine
    let navigate = useNavigate();

    const dispatch = useDispatch();
    const salvaLibro = () =>{
        if (titolo == '') {
            setErrore('Titolo mancante');
            return;
        }
        if (autore == '') {
            setErrore('Autore mancante');
            return;
        }
        dispatch(addProdotto(titolo, autore, pagine));
        setTitolo('');
        setAutore('');
        setPagine('');
        // torna alla pagina principale
        navigate('/'); 
    }
    return <div className="form-prodotto">
        <div><label>Titolo</label><input value={titolo} onChange={(e) => setTitolo(e.target.value)}/></div>
        <div><label>Autore</label><input value={autore} onChange={(e) => setAutore(e.target.value)}/></div>
        <div><label>pagine</label><input value={pagine} onChange={(e) => setPagine(e.target.value)}/></div>
        <br/>
        <button onClick={salvaLibro}>Salva</button>

        {errore && <div className="errore">{errore}</div>}
    </div>
}