import { Route, Routes } from "react-router-dom";
import ListaProdotti from "./ListaProdotti";
import FormProdotto from "./FormProdotto";
import Prodotto from "./Prodotto";
import Menu
 from "./Menu";
const App  = () => {
    return <div>
        <Menu/>
        <Routes>
            <Route path="/" element={<ListaProdotti/>}/>
            <Route path="/prodotto" element={<FormProdotto/>}/>
            <Route path="/prodotto/:id" element={<Prodotto/>}/>
        </Routes>
    </div>
}

export default App;