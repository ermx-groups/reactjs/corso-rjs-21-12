import React from 'react';
import { createRoot } from 'react-dom/client';

import './index.css';
import App from './shop/componenti/App';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./shop/store"

let reactObject = createRoot(document.getElementById('root'));

reactObject.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <App />
      </Provider>   
    </BrowserRouter>
  </React.StrictMode>
);

