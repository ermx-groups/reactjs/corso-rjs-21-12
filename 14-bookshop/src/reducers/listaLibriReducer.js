const ADD_LIBRO = 'lista-libri/add';
const DEL_LIBRO = 'lista-libri/del';
const UPD_LIBRO = 'lista-libri/upd';

const listaLibriReducer = (pstate = null, action) => {
    if (pstate === null) {
        pstate = JSON.parse(localStorage.getItem('lista-libri')) || [];
    }
    switch(action.type) {
        case ADD_LIBRO: {
            // genera un id random univoco
            let newLibro = {...action.payload, id:new Date().valueOf()};
            let nstate = [...pstate, newLibro];
            return nstate;
        }
        case DEL_LIBRO: {
            let nstate = [...pstate].filter(item => parseInt(item.id) !== parseInt(action.payload));
            return nstate;
        }
        case UPD_LIBRO: {
            console.log(action.payload);
            let nstate = [...pstate].map(item=> {
                // se l'oggetto corretto è quello da modificare
                if (parseInt(item.id) === parseInt(action.payload.id)) {
                    // lo copio e ne modifico i campi presenti sul payload
                    return {...item, ...action.payload}
                } else {
                    // altrimenti lo restituisco inalterato
                    return item
                }
            });
            return nstate;
        }
    }
    return pstate;
}

export default listaLibriReducer;

const actionAddLibro = (titolo, pagine) => {
    return {
        type : ADD_LIBRO,
        payload : {
            titolo : titolo,
            pagine : pagine
        } 
    }
}

const actionDelLibro = (id) => {
    return {
        type : DEL_LIBRO,
        payload : id
    }
}

const actionUpdateLibro = (id, titolo, pagine) => {
    return {
        type : UPD_LIBRO,
        payload : {
            id, titolo, pagine
        }
    }
}

export {actionAddLibro, actionDelLibro, actionUpdateLibro}