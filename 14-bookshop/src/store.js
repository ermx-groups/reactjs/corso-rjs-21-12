import {createStore, combineReducers} from "redux";
import listaLibriReducer from "./reducers/listaLibriReducer";

const store = createStore (combineReducers(
    {listaLibri : listaLibriReducer}
));

store.subscribe(() => localStorage.setItem('lista-libri', JSON.stringify(store.getState().listaLibri)))

export default store;