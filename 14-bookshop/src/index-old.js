import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import {Provider} from "react-redux";
import App from './componenti/App';
import store from "./store";
import {BrowserRouter} from "react-router-dom";

let reactObject = createRoot(document.getElementById('root'));

reactObject.render(
  <React.StrictMode>
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>
  </React.StrictMode>
);

