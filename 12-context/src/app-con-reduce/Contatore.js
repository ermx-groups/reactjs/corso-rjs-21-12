
import {useState} from 'react';
import './Contatore.css';

function Contatore (props) {
    return (<div className="Contatore">
        <div className="label">{props.label} : </div>
        <button onClick={props.onDecremento}>-</button>
        <input type="text" size="5" style={{textAlign:"right"}} value={props.value} onChange={()=>{}}/>
        <button onClick={props.onIncremento}>+</button>
    </div>)
}

export default Contatore;