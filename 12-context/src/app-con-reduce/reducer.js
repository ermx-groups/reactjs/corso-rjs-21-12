
const numGambe = {
    pecore : 4,
    cigni : 2,
    galline : 2,
    cani : 4
}

function calcolaGambe(stato) {
    let arrayPropStd = Object.keys(numGambe); // => [pecore, cigni, galline, cani]
    // [pecore, cigni, galline, cani].map(pecore => 2 * 4) --> [2*4, 1*2, ....]
    let arrayNumGambe = arrayPropStd.map(field => stato[field] * numGambe[field]);
    return arrayNumGambe.reduce((gambeAnimale, totale) => gambeAnimale + totale, 0);
}

function checkGallineCigni(stato, field, step) {
    if ( field === 'galline' || field ==='cigni') { // ['galline', 'cigni'].indexOf(field) > -1) {
        let somma = stato.galline + stato.cigni + step;
        if (somma > 15) {
            if(field == 'galline') {
                if (stato.cigni < step) {
                    throw "Troppi pochi cingi";
                }
                stato.cigni -= step;
            } else {
                if (stato.galline < step) {
                    throw 'Troppe poche galline';
                }
                stato.galline -= step;
            }
        }
    }
}

function checkStato(stato, field, step) {
    if (stato[field] + step < 0) {
        throw 'Valori negativi non permessi';
    }
    // Richiesta 1
    console.log(stato.pecore, step);
    if (field === 'pecore' && stato[field] + step > 10 ) {
        console.log('.....');
        throw 'Pecore massime : 10';
    }
    // Richeista 2
    if (field === 'galline' && stato[field] + step > 10 ) {
        throw 'Galline massime : 10';
    }
    // Richiesta 3
    checkGallineCigni(stato, field, step);
    // richiesta 4
    if (field === 'pecore') {
        if (!checkGallineCigni(stato, 'galline', step)) return false;
        if (step > 0) {
            if (stato.galline + step > 10 ) {
                throw 'Per comprare una pecora devi diminuire le galline';
            }
            stato.galline += step;
        }
    }

    return true;
}

function reducer(stato, action) {
    
    switch(action.type) {
        case 'INIT' : {
            let newStato = {...stato, error:null};
            newStato.totGambe = calcolaGambe(newStato);
            return newStato;
        }
        case 'MOVE': {
            let newStato = {...stato, error:null};
            let field = action.field;
            let step = action.step || 0;
            if (step==0) step = 1;
            try {
                checkStato(newStato, field, step);
            } catch (error) {
                return {...stato, error: error};
            } finally {
            }
            newStato[field] = stato[field] + step;
            newStato.totGambe = calcolaGambe(newStato);
            if (newStato.totGambe > 80) return stato;
            return newStato;
        }
        default : 
            return stato;
        }
}

export default reducer;


function actionMove(field, others) {
    return {type:'MOVE', payload : {...others, field: field}};
}

function actionInit() {
    return {type:"INIT"};
}
export {actionMove, actionInit}