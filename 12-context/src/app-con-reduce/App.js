import { useEffect, useReducer } from "react";
import Contatore from "./Contatore";
import reducer, {actionMove, actionInit} from "./reducer";

function Errore(props) {
    return <h2>{props.message}</h2>
}

function App(props) {


    let [statoApp, dispatch] = useReducer(reducer, {pecore:9, galline:7, cigni:5, cani:2});
    useEffect(()=>dispatch(actionInit()), []);

    return <div>
        <h3>Nella vecchia fattoria</h3>
        <Contatore label="Pecore" value={statoApp.pecore}  onDecremento={()=>dispatch({type:"MOVE", field:"pecore", step:-1})} onIncremento={()=>dispatch({type:"MOVE", field:"pecore"})}/>
        <Contatore label="Galline" value={statoApp.galline}  onDecremento={()=>dispatch({type:"MOVE", field:"galline", step:-1})} onIncremento={()=>dispatch({type:"MOVE", field:"galline"})}/>
        <Contatore label="Cigni" value={statoApp.cigni} onDecremento={()=>dispatch({type:"MOVE", field:"cigni", step:-1})} onIncremento={()=>dispatch({type:"MOVE", field:"cigni"})}/>
        <Contatore label="Cani" value={statoApp.cani} onDecremento={()=>dispatch({type:"MOVE", field:"cani", step:-1})} onIncremento={()=>dispatch({type:"MOVE", field:"cani"})}/>
        <div>
            <b>Totale Gambe : </b>
            <span>{statoApp.totGambe}</span>
        </div>
        <div>{statoApp.error && <Errore message={statoApp.error}/>}</div>
    </div>
}

export default App;