import { useContext } from "react";
import UiContext from "../../UiContext";

const ItemButter = ({item}) => {
    let ctx = useContext(UiContext);
    const clickItem = () => {
        ctx.removeButter(item.id);
        console.log(item);
    }
    return <div className="item">
        <button className={ctx.stile} onClick={clickItem} type="button">X</button><div className="name">{item.type}</div>
    </div>
}

export default ItemButter;