import Item from "./ItemTopping";
import './ListTopping.scss';
const TL = ({lista}) => {
    return <div className="topping">
        <h3>Topping</h3>
        {lista.map(item => <Item key={item.id} {...item}/>)}
    </div>
}

export default TL;