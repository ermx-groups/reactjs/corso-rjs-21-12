import ItemButter from "./ItemButter";
import "./ListButter.scss";

const BL = ({lista}) => {
    return <div className="butter">
        <h3>Butters</h3>
        {lista.map(item => <ItemButter key={item.id} item={item}/>)}
    </div>
}

export default BL;