import { useContext } from "react";
import UiContext from "../../UiContext";

const ItemTopping = ({id, type}) => {
    let ctx = useContext(UiContext);
    const clickItem = () => {
        ctx.removeTopping(id);
        console.log(id);
    }
    return <div className="item">
        <button className={ctx.stile}  onClick={clickItem} type="button">X</button><div className="name">{type}</div>
    </div>
}

export default ItemTopping;