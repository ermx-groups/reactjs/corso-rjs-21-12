import Testata from './Testata';
import ButterList from './ListButter';
import ToppingList from './ListTopping';

import './index.style.css';

const ViewData = ({data}) => {
    let {id, type, name, ppu, batters, topping} = data;
    return <div className="view-data">
        <Testata {...{id, type, name, ppu}}/>
        <ButterList lista={batters}/>
        <ToppingList lista={topping}/>
    </div>
}

export default ViewData;