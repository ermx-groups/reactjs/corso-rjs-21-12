import Field from './Field';
import './Testata.scss';

const Testata = ({id, type, name, ppu}) => {
    return  <div className="testata">
        <h3>Testata</h3>
        <Field name="ID" value={id}/>
        <Field name="Type" value={type}/>
        <Field name="Name" value={name}/>
        <Field name="PPU" value={ppu}/>
    </div>
}

export default Testata;