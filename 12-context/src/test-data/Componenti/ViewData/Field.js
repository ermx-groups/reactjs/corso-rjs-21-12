const Field = ({name, value}) => {
    return <div className="field"><label>{name}</label> : <code>{value}</code></div>
}

export default Field;