import {useState, useEffect} from 'react';
import Loading from './Componenti/Loading';
import ViewData from './Componenti/ViewData';

import UiContext from './UiContext';

const App =  () => {
    const [data, setData] = useState(null);
    const [stile, setStile] = useState('dark');

    const cambiaStile = () => {
        if (stile === 'dark') {
            setStile('celeste');
        } else {
            setStile('dark');
        }
    }
    const richiediDati = () => {
        fetch('/data.json')
        .then(res => res.json())
        .then(data => setData(data));
    }

    useEffect(()=>{
        setTimeout(richiediDati, 1000);
    }, []);

    const removeButter = (id) => {
        let newData = {...data};
        let newBatters = newData.batters.filter(item => item.id !== id);
        newData.batters = newBatters;
        setData(newData);
    }

    const removeTopping = (id) => {
        let newData = {...data};
        let newTopping = newData.topping.filter(item => item.id !== id);
        newData.topping = newTopping;
        setData(newData);
    }

    // const [isLoading, setIsLoading] = useState(true);
    return ((data === null) ? <Loading/> : <div>
            <button onClick={cambiaStile}>Cambia stile [{stile}]</button>
            <UiContext.Provider value={{stile, removeButter, removeTopping}}>
                <ViewData data={data} />
            </UiContext.Provider>
        </div>)
}


export default App;