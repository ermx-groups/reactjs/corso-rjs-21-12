import {useContext} from 'react';
import AppContext from "./AppContext";

const Item= ({id, name}) => {
    let {handlerRemoveItem} = useContext(AppContext);
    return <article>
        <h3>{name}</h3>
        <button className="button button-remove" onClick={()=>handlerRemoveItem(id)}>x</button>
    </article>
}

export default Item;