import {useReducer} from 'react';
import './App.css';

import ViewLista from './ViewLista';
import initialState from './state';
import reducer from './reducer';
import {removeItem} from './actions';
import Ctx from '../test-reduce/AppContext';

function App() {

  const [state, dispatch] = useReducer(reducer, initialState);
  let a = 15;
  const handlerRemoveItem = (id) => dispatch({type:'remove/item', itemID : id});
  return (
    <Ctx.Provider value={{state, handlerRemoveItem}}>
      <div className="App">
        <ViewLista/>
      </div>
    </Ctx.Provider>
  );
}



export default App;
