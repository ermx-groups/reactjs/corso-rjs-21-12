import {useContext} from 'react';
import Item from './Item';
import AppContext from './AppContext';

const ViewLista = () => {
    let info = useContext(AppContext);
    return (
        <section>
        {
            info.state.lista.map(item=><Item key={item.id} {...item}/>)
        }
        </section>
    );
}

export default ViewLista;