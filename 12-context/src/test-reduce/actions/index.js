const  removeItem = (id) => {
    return {
        type : 'lista/remove',
        payload : id
    }
}

export { removeItem };