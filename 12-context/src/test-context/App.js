import { useState, createContext } from 'react';
import './App.css';
import data from './data.json';
import Ctx from './AppContext';
import UserCtx from './UserContext';
import ViewLista from './ViewLista';

function App() {
  let [lista, setLista] = useState(data);
  const handlerRemoveItem = (id) => {
    setLista(lista.filter(item => item.id != id));
  }
  return (
    <>
      <UserCtx.Provider value={{ nome: "Ciccio" }}>
        <Ctx.Provider value={{ lista: lista, handlerRemoveItem }}>
          <div className="App">
            <ViewLista />
          </div>
        </Ctx.Provider>
        <Ctx.Provider value={{}}>
          <ViewLista />
        </Ctx.Provider>
        <ViewLista />
      </UserCtx.Provider>
    </>
  );
}



export default App;
