import {useContext} from 'react';
import Item from './Item';
import AppContext from './AppContext';

const ViewLista = () => {
    let info = useContext(AppContext);
    return (
        <section>
        {info.lista.map((item, inedx)=><Item key={item.id} {...item}/>)}
        </section>
    );
}

export default ViewLista;