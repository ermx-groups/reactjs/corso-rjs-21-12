import {useContext} from 'react';
import AppContext from "./AppContext";
import userContext from './UserContext';

const Item= ({id, name}) => {
    let {handlerRemoveItem} = useContext(AppContext);
    let user = useContext(userContext);
    console.log(user);
    return <article>
        <h3>{name} : {user.nome}</h3>
        <button className="button button-remove" onClick={()=>handlerRemoveItem(id)}>x</button>
    </article>
}

export default Item;