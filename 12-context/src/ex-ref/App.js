import React from "react";

import {LogButtonClicks} from './Button';

const App = () => {
    return <div>
        <LogButtonClicks />
    </div>
}

export default App;