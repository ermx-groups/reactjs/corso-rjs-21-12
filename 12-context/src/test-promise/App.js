import React, {useState, useEffect} from 'react';

function sum(a,b) {
  return new Promise((success, error)=>{
    let res = a+b;
    setTimeout(()=>success(res), 3000);
  })
}

function App(props) {
  let [val, setVal] = useState('...sto calcolando ...');
  let [resPromise, setResPromise] = useState(null);
  let {a, b} = props;

  useEffect(()=>{    
    let ap = sum(a, b);
    ap.then((res)=>{
      console.log(res);
      setVal(res);
    });
    setResPromise(ap);
  }, []);

  useEffect(()=>{
    if (resPromise){
      resPromise.then((res) => {
        console.log('val' ,val, res);
        setVal(res+1);
      })    
    }
  }, [val]);


  return (
   <div>{a} + {b} = <span>{val}</span></div>
  );
}

export default App;
