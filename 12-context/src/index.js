import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './app-con-reduce/App';

ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
);


