## Esercizio

Creare una Applicazione che mostri i dati presenti nel file public/data.json.
Costruire un layout che rispetti la struttura del file data.json definendo gli opportuni componenti.
I dati saranno disponibili attraverso il servizio <code>fetch('/data.json')</code> e quindi caricati nell'interfaccia dopo il primo rendering.

### Esercizio 2

Completare l'esercitazione della directory "app-con-reduce". 
Ogni contatore deve poter richiamare le action configurate nel reducer in modo appropriato.
Inoltre occorre che l'invocazione di ogni action deve ricalcolare anche il campo "totaleGambe" dello stato di modo che App debba solo mostrare questo risultato (non lo deve calcolare).

Nel reducer definire anche tutte le funzioni di utilità che generano le action necessarie (tipo actionMove).

Infine ... quando funziona la configurazione semplice ... aggiornare il codice affinchè si rispettino questi vincoli:
- Non ci possono essere più di 10 pecore
- Non ci possono essere più di 10 galline
- La somma tra galline e cigni non può superare 15 ... quindi se si aumentano i cigni e si supera questa soglia, occorre diminuire le galline ... fin che si può (e viceversa).
- Se si acquista una pecora, in regalo c'è sempre una gallina. Se si toglie una pecora ... la gallina rimane.
- Non ci possono essere più di 80 zampe in tutto.

Infine ... renderlo presentabile :)

Buon lavoro.