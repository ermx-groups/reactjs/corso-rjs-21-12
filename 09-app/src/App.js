import React from "react";
import Form from './Form';
import Rubrica from './Rubrica';



class Login extends React.Component {
  constructor(props) {
  super(props);
  this.state = {
    username : '',
    password : ''
  }
  }

  render () {
  return  <>
  
    <div className="boxform">  
      <div>   
        <h1>Effettua il Login</h1>       
       <input name="username" type="text" placeholder="Username" value={this.state.username} onChange={(e) =>this.setState({username:e.target.value})}/>
     </div>
    <div>
    <input name="password" type="password" placeholder="Password" value={this.state.password} onChange={(e) =>this.setState({password:e.target.value})}/>
    </div>
    <div>
    <button type="button" value="Accedi" onClick={() =>this.props.onLogin(this.state.username, this.state.password)}>Accedi</button>
    </div>
   </div>
   </>
  }
  
}





const getData = () => {
  return [
    {username: 'admin', password: 'admin'},
    {username: 'guest', password: 'guest'}
  ]
}


class App extends React.Component {
   constructor(props) {
     super(props);
     this.handlerConfirm = this.handlerConfirm.bind(this);
     this.handlerLogin = this.handlerLogin.bind(this);
     this.state = {rubrica : []};
     this.state = {user : null};

   }

  handlerConfirm(item) {
     let nr = [...this.state.rubrica];
     nr.push(item);
     this.setState({rubrica: nr});
  }

  handlerLogin(username, password) {
    let userList = getData();
    let userAct = userList.find(user => user.username === username && user.password === password);
    if(userAct.lenght === 0) {
      this.setState({user:null, error: 'Utente non trovato'});
    }
     else {
      this.setState({user: userAct[0], error:''});
     }
    
  }

  viewError() {
    return <>
      <h3>Errore</h3>
      <b>{this.state.error}</b>
    </>
  }


  render () {

    
     return (
      <div className="App">
        
        {
          this.state.user == null ? 
            <Login onLogin={(login, password)=>this.handlerLogin(login, password)}/> : 
            <>
              <Form onLogin={() => this.handlerLogin}/>;
              <Rubrica dati={this.state.rubrica}/>;
            </>
        }
        {this.state.error && this.viewError()}
      </div>
    );
      
  
}
  }


   /* <>
    <Login onLogin={(username, password) => this.handlerLogin(username, password)}/>;
    
    </>

    let content = <Login onLogin={(username, password) => this.handlerLogin(username, password)}/>;
    if (this.state.username === 'admin' && this.state.password === 'admin') {
     content =<>
      <Form onLogin={() => this.handlerLogin}/>
      <Rubrica dati={this.state.rubrica}/></>
    }
   return (
     <>
     {content}
  
   
    </>
    );
  }*/


export default App;