# Il linguaggio Javascript

Scopi e utilizzi di un linguaggio di programmazione lato client.

Caratteristiche di un linguaggio di programmazione (variabili, comandi ed espressioni)

## Proposte di studio 
- Base https://www.html.it/guide/guida-javascript-di-base/
- Approfondimenti https://www.html.it/javascript/
- Esempi https://www.html.it/guide/guida-javascript-per-esempi/

## Esercitazione
Utilizzare la directory **esercitazione** per mettere gli esercizi proposti di seguito. Ogni esercizio deve essere inserito in una cartella dedicata.

1. Esercizio fatto a lezione per vedere come manipolare il documento direttamente da js.
E ora tocca a voi ...

2. Creare una pagina con un input ed un pulsante. La pressione del pulsante visualizza un alert con il contenuto che l'utente ha inserito nell'input.

3. Creare una pagina con una lista elementi ed un pulsante. La pressione di un pulsante apre un "prompt" che invita a l'utente ad immettere un valore. Questo valore verrà inserito in fondo alla lista. Inoltre ogni elemento della lista ha un pulsante [x] che permette di poter eliminare l'elemento dalla lista, chiedendo conferma dell'operazione.

4. Creare una form di login con i campi username e password ed il tasto Accedi che verifchi se l'utente ha inserito come username la strina "admin" e come password "root".
In caso affermativo la login deve sparire e deve apparire una bella scritta "Benvenuto!" in centro pagina sia verticale che orizzontale.
In caso negativo deve azzerare i valori inseriti e far apparire un piccolo messaggio di avvertimento ("accesso non effettuato") che copra la login (così da non renderla utilizzabile) per 5 secondi.
Scaduto il tempo, il messaggio sparisce così da permettere all'utente di poter ritentare l'accesso.
Mi raccomando .. grafica bella :) 