# Primo approccio a ReactJS

ReactJS è una libreria javascript che si propone come strumento per la creazione di SPA, ovvero le applicazioni a singola pagina.

Prerequisiti per poter capire le dinamiche che si scaturiscono durante il processo di esecuzione di una SPA sono:
1. cos'è un DOM (https://www.html.it/guide/guida-dom/)
2. gestione degli eventi (https://www.html.it/pag/15200/gli-eventi/)
3. cos'è un documento XML (documentazione proposta in 00-html)
4. guida reactjs (https://www.html.it/guide/react-la-guida/)
5. tutorial (https://it.reactjs.org/tutorial/tutorial.html)

### Principi base
- Cos'è un componente (tramite funzioni o classe)
- Cosa sono le proprietà

### Esercizi

1. Provare a realizzare la form di login utilizzando i componenti React.
   Passare le label tramite le props.